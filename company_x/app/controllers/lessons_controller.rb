class LessonsController < ApplicationController
 def index
 	 @lessons = Lesson.all
 end

 def complete
 	@lesson = Lesson.find(params[:id])
 	@lesson.update(lesson_complete_params)
 	redirect_to 'index'

 end

 private
  def lesson_complete_params
  	params.require(:article).permit(:complete)	
  end

end
