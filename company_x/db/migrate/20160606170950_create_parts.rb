class CreateParts < ActiveRecord::Migration
  def change
    create_table :parts do |t|
      t.boolean :completed
      t.integer :number

      t.timestamps null: false
    end
  end
end
